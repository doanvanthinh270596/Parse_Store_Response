/*
 * Function.c
 *
 *  Created on: Aug 8, 2017
 *      Author: Doan Van Thinh
 */
//****************************//
#include "Function.h"
#include "database.h"
//****************************//
uint8_t buffer[200][300],length[200];
uint8_t stt=0;
uint8_t i,j,m,n;
uint8_t TagName_1,TagName_2,TagName_3;

const uint8_t arrayTVL[1000]={0x6F, 0x0C, 0x4F, 0x07, 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x10, 0x87, 0x01, 0x01, 0x84, 0x0E, 0x32,
		                      0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x5A, 0x08, 0x54, 0x13,
							  0x33, 0x90, 0x00, 0x00, 0x15, 0x13, 0x8C, 0x0E, 0x95, 0x05, 0x9F, 0x02, 0x06, 0x9F, 0x34, 0x03, 0x9F,
							  0x33, 0x03, 0x9F, 0x37, 0x04 };
//**************************************************//
void CheckTag(uint8_t byte_1,uint8_t byte_2,uint8_t byte_3){
    switch(byte_1){

          case 0x94: stt=0;
              break;
          case 0x82: stt=1;
              break;
          case 0x50: stt=2;
              break;
          case 0x5A: stt=3;
              break;
          case 0x87: stt=4;
              break;
          case 0x8F: stt=5;
              break;
          case 0x8C: stt=6;
              break;
          case 0x8E: stt=7;
              break;
          case 0x84: stt=8;
              break;
          case 0xA5: stt=9;
              break;
          case 0x6F: stt=10;
              break;
          case 0x90: stt=11;
              break;
          case 0x92: stt=12;
              break;
          case 0x70: stt=13;
              break;
          case 0x80: stt=14;
              break;
          case 0x77: stt=15;
              break;
          case 0x95: stt=16;
              break;
          case 0x56: stt=17;
              break;
          case 0x57: stt=18;
              break;
          case 0x9A: stt=19;
              break;
          case 0x9C: stt=20;
              break;
          case 0x5F: switch(byte_2){
                           case 0x57: stt=21;
                               break;
                           case 0x25: stt=22;
                               break;
                           case 0x24: stt=23;
                               break;
                           case 0x34: stt=24;
                               break;
                           case 0x28: stt=25;
                               break;
                           case 0x2D: stt=26;
                               break;
                           case 0x30: stt=27;
                               break;
                           case 0x2A: stt=28;
                               break;
                           case 0x36: stt=29;
                               break;
                     }
              break;
          case 0x9F: switch(byte_2){
                           case 0x01: stt=30;
                                break;
                           case 0x40: stt=31;
                                break;
                           case 0x02: stt=32;
                                break;
                           case 0x03: stt=33;
                                break;
                           case 0x5D: stt=34;
                                break;
                           case 0x26: stt=35;
                                break;
                           case 0x42: stt=36;
                                break;
                           case 0x44: stt=37;
                                break;
                           case 0x12: stt=38;
                                break;
                           case 0x36: stt=39;
                                break;
                           case 0x07: stt=40;
                                break;
                           case 0x08: stt=41;
                                break;
                           case 0x09: stt=42;
                                break;
                           case 0x27: stt=43;
                                break;
                           case 0x60: stt=44;
                                break;
                           case 0x61: stt=45;
                                break;
                           case 0x34: stt=46;
                                break;
                           case 0x51: stt=47;
                                break;
                           case 0x5B: stt=48;
                                break;
                           case 0x5E: stt=49;
                                break;
                           case 0x54: stt=50;
                                break;
                           case 0x5C: stt=51;
                                break;
                           case 0x5F: stt=52;
                                break;
                           case 0x6F: stt=53;
                                break;
                           case 0x7D: stt=54;
                                break;
                           case 0x7F: stt=55;
                                break;
                           case 0x4C: stt=56;
                                break;
                           case 0x46: stt=57;
                                break;
                           case 0x47: stt=58;
                                break;
                           case 0x48: stt=59;
                                break;
                           case 0x1E: stt=60;
                                break;
                           case 0x0D: stt=61;
                                break;
                           case 0x0E: stt=62;
                                break;
                           case 0x0F: stt=63;
                                break;
                           case 0x10: stt=64;
                                break;
                           case 0x11: stt=65;
                                break;
                           case 0x32: stt=66;
                                break;
                           case 0x4D: stt=67;
                                break;
                           case 0x6D: stt=68;
                                break;
                           case 0x15: stt=69;
                                break;
                           case 0x7C: stt=70;
                                break;
                           case 0x16: stt=71;
                                break;
                           case 0x4E: stt=72;
                                break;
                           case 0x7E: stt=73;
                                break;
                           case 0x64: stt=74;
                                break;
                           case 0x67: stt=75;
                                break;
                           case 0x50: stt=76;
                                break;
                           case 0x24: stt=77;
                                break;
                           case 0x62: stt=78;
                                break;
                           case 0x65: stt=79;
                                break;
                           case 0x38: stt=80;
                                break;
                           case 0x70: stt=81;
                                break;
                           case 0x71: stt=82;
                                break;
                           case 0x72: stt=83;
                                break;
                           case 0x73: stt=84;
                                break;
                           case 0x74: stt=85;
                                break;
                           case 0x63: stt=86;
                                break;
                           case 0x66: stt=87;
                                break;
                           case 0x4B: stt=88;
                                break;
                           case 0x4A: stt=89;
                                break;
                           case 0x33: stt=90;
                                break;
                           case 0x1A: stt=91;
                                break;
                           case 0x1C: stt=92;
                                break;
                           case 0x1D: stt=93;
                                break;
                           case 0x35: stt=94;
                                break;
                           case 0x6E: stt=95;
                                break;
                           case 0x1F: stt=96;
                                break;
                           case 0x6B: stt=97;
                                break;
                           case 0x20: stt=98;
                                break;
                           case 0x53: stt=99;
                                break;
                           case 0x21: stt=100;
                                break;
                           case 0x69: stt=101;
                                break;
                           case 0x37: stt=102;
                                break;
                           case 0x6A: stt=103;
                                break;
                           case 0x75: stt=104;
                                break;
                           case 0x76: stt=105;
                                break;
                           case 0x77: stt=106;
                                break;
                           case 0x78: stt=107;
                                break;
                           case 0x79: stt=108;
                                break;
                     }
              break;
          case 0xBF: switch(byte_2){
                           case 0x0C: stt=109;
                               break;
                     }
              break;
          case 0xDF: switch(byte_2){
                           case:
                        	   break;


                           case 0x81: switch(byte_3){
                                            case :
                                    	        break;
                                      }
                               break;
                           case 0x83: switch(byte_3){
                                            case:
                                            	break;
                                      }
                               break;

                     }
              break;







               }
   }



void GetAndStoreTag(uint8_t* TLV){
	i=0;
	while(TLV[i]!=0){

	TagName1=TLV[i];
	TagName2=TLV[i+1];
	TagName3=TLV[i+2];
	switch(TagName_1){
	      case 0x94:case 0x82:case 0x50:case 0x5A:case 0x87:case 0x8F:case 0x8C:case 0x8E:case 0x84:case 0xA5:
          case 0x6F:case 0x90:case 0x92:case 0x70:case 0x80:case 0x77:case 0x95:case 0x56:case 0x57:case 0x9A:case 0x9C:
        	  GetTag_1();
		      break;
          case 0x5F: switch(TagName_2) {
                           case 0x57:case 0x25:case 0x24:case 0x34:case 0x28:case 0x2D:case 0x30:case 0x2A:case 0x36:
                    	       GetTag_2();
                               break;
                     }
        	  break;
          case 0x9F: switch(TagName_2){
                           case 0x01:case 0x40:case 0x02:case 0x03:case 0x5D:case 0x26:case 0x42:case 0x44:case 0x12:case 0x36:
                           case 0x07:case 0x08:case 0x09:case 0x27:case 0x60:case 0x61:case 0x34:case 0x51:case 0x5B:case 0x5E:
                           case 0x54:case 0x5C:case 0x5F:case 0x6F:case 0x7D:case 0x7F:case 0x4C:case 0x46:case 0x47:case 0x48:
                           case 0x1E:case 0x0D:case 0x0E:case 0x0F:case 0x10:case 0x11:case 0x32:case 0x4D:case 0x6D:case 0x15:
                           case 0x7C:case 0x16:case 0x4E:case 0x7E:case 0x64:case 0x67:case 0x50:case 0x24:case 0x62:case 0x65:
                           case 0x38:case 0x70:case 0x71:case 0x72:case 0x73:case 0x74:case 0x63:case 0x66:case 0x4B:case 0x4A:
                           case 0x33:case 0x1A:case 0x1C:case 0x1D:case 0x35:case 0x6E:case 0x1F:case 0x6B:case 0x20:case 0x53:
                           case 0x21:case 0x69:case 0x37:case 0x6A:case 0x75:case 0x76:case 0x77:case 0x78:case 0x79:
                               GetTag_2();
                               break;
                    }
              break;
          case 0xBF: if(TagName_2==0x0C) GetTag_2();
              break;
          case 0xDF: switch(TagName_2){
                           case:
                        	   break;
                           case 0x81: switch(TagName_3){
                                            case:
                                            break;
                                      }
                               break;
                     }
	}
	}
}
//**************************************************//
void GetTag_1(){
    CheckTag(TagName_1,0,0); // kiem tra
    buffer[stt][0]= TagName_1; // luu gia tri tag
    length[stt]= TLV[i+1];
    buffer[stt][1]=length[stt];// luu gia tri do dai
    for(j=0;j<length[stt];j++){
   	buffer[stt][j+2]=TLV[i+j+2];  //*********????********
    }
    i+=(length[stt]+2);
}
void GetTag_2(){
    CheckTag(TagName_1,TagName_2,0);
    buffer[stt][0]=TagName_1;
    buffer[stt][0]=TagName_2;
    length[stt]=TLV[i+1];
    buffer[stt][1]=length[stt];// luu gia tri do dai
    for(j=0;j<length[stt];j++){
        buffer[stt][j+3]=TLV[i+j+3];
       }
    i+=(length[stt]+3);
}
void GetTag_3(){
	CheckTag(TagName_1,TagName_2,TagName_3);
	buffer[stt][0]=TLV[i];
	buffer[stt][1]=TLV[i+1];
	buffer[stt][2]=TLV[i+2];
	length[stt]=TLV[i+3];
	buffer[stt][3]=TLV[i+3];
	for(j=0;j<lenhth[stt];j++){
		buffer[stt][j+4]=TLV[i+j+4];
	}
	i+=(length[stt]+4);
}
//**************************************************//

Boolean IsKnown(Tag T){
  if(T<180&&T>=0) return TRUE;
  else return FALSE;
}
Boolean IsPresent(Tag T) {
  if(buffer[T][0]==0) return FALSE;
  else return TRUE;
}
Boolean IsNotPresent(Tag T){
       if(IsPresent(T)==TRUE) return FALSE;
       else return TRUE;
}
Boolean IsEmpty(Tag T){
  if(IsPresent(T)==TRUE && length[T]==0) return TRUE;
  else return FALSE;
}
Boolean IsNotEmpty(Tag T){
        if(IsEmpty(T)==TRUE) return FALSE;
        else return TRUE;
}
void Reset(uint8_t* _buffer,uint8_t* LENGTH){
	uint8_t g,h;
  for(g=0;g<200;g++){
	  for(h=0;h<300;h++){
		  _buffer[g][h]=0;
	  }
  }
  for(g=0;g<200;g++){
	  LENGTH[g]=0;
  }

}
/*
Boolean ParseAndStoreCardResponse(uint8_t* arrayTVL,uint16_t length_arryTVL){
	Boolean TLV_Encoding_Error=FALSE;



/*
	if(TLV_Encoding_Error==TRUE) return FALSE;
	else {
		for(;;){
		   if(){
			 if(){
			   if(){

			   }
			   else{

			   }
			 else{
				 if(){

				 }
				 else{
					 if(){
					   if(){

					   }
					 }
				 }
			 }

			 }
		   }
		}
		return TRUE;
	}
}

*/
























