/*******************************************************************************
 *  (C) Copyright 2009 Molisys Solutions Co., Ltd. , All rights reserved       *
 *                                                                             *
 *  This source code and any compilation or derivative thereof is the sole     *
 *  property of Molisys Solutions Co., Ltd. and is provided pursuant to a      *
 *  Software License Agreement.  This code is the proprietary information      *
 *  of Molisys Solutions Co., Ltd and is confidential in nature.  Its use and  *
 *  dissemination by any party other than Molisys Solutions Co., Ltd is        *
 *  strictly limited by the confidential information provisions of the         *
 *  Agreement referenced above.                                                *
 ******************************************************************************/

/**
 * @author: Doan Van Thinh
 * @email: Doanvanthinh@gmail.com
 *  Created on: Aug 8, 2017
 *
 */

#ifndef FUNCTION_FUNCTION_H_
#define FUNCTION_FUNCTION_H_

/********** Include section ***************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "database.h"
//********************************//
/********** Constant  and compile switch definition section *******************/


//********************************//
/********** Type definition section *******************************************/

/********** Function declaration section **************************************/


Data_Status_Tag Read_Status(Tag T);

Data_Boolean IsKnown(Tag T);

Data_Boolean IsPresent(Tag T);

Data_Boolean IsNotPresent(Tag T);

Data_Boolean IsEmpty(Tag T);

Boolean IsNotEmpty(Tag T);

Boolean ParseAndStoreCardResponse(uint8_t* arrayTVL,uint16_t length_arryTVL);

Status_leng GetLength(Tag T);

uint8_t GetTag(uint8_t* arrayTLV,Tag T);

void GetTag_1(uint8_t* TLV);

void GetTag_2(uint8_t* TLV);

void GetTag_3(uint8_t* TLV);

void CheckTag(uint8_t* TLV);

void GetAndStoreTag(uint8_t* TLV);


//********************************//




#endif /* _FUNCTION_H_ */
